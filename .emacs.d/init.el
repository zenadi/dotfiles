(load "~/.emacs.d/better-defaults")
;(add-to-list 'load-path (expand-file-name "~/.emacs.d/irony-mode/elisp/"))

(require 'package)
(add-to-list 'package-archives
             '("org" . "http://orgmode.org/elpa/") t)
(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org/packages/") t)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.milkbox.net/packages/") t)

;;; from purcell/emacs.d
(defun require-package (package &optional min-version no-refresh)
  "Install given PACKAGE, optionally requiring MIN-VERSION.
If NO-REFRESH is non-nil, the available package lists will not be
re-downloaded in order to locate PACKAGE."
  (if (package-installed-p package min-version)
      t
    (if (or (assoc package package-archive-contents) no-refresh)
        (package-install package)
      (progn
        (package-refresh-contents)
        (require-package package min-version t)))))

(package-initialize)

;(require-package 'color-theme)
;(color-theme-initialize)
;(color-theme-subtle-hacker)

;;;;; AUTOCOMPLETION ;;;; BEGIN
;(require-package 'auto-complete)
;(require-package 'yasnippet)
;(require 'auto-complete)
;(require 'yasnippet)

;(require 'irony)
;(irony-enable 'ac)

;(defun cpp-hooks ()
  ;"Enable the hooks in the preferred order: 'yas -> auto-complete -> irony'."
  ;be cautious, if yas is not enabled before (auto-complete-mode 1), overlays
  ;*may* persist after an expansion.
  ;(yas/minor-mode-on)
  ;(auto-complete-mode t)

  ;avoid enabling irony-mode in modes that inherits c-mode, e.g: php-mode
  ;(when (member major-mode irony-known-modes)
    ;(irony-mode t)))
;(add-hook 'c++-mode-hook 'cpp-hooks)
;(add-hook 'c-mode-hook 'cpp-hooks)


;;;;; AUTOCOMPLETION ;;;; END

;;;;; ORG ;;;;; BEGIN
;(require-package 'org)
;(require-package 'org-plus-contrib)
;(require 'org)
;;;;; ORG ;;;;; END

;;;;; EVIL ;;;;; BEGIN
;(require-package 'evil)

;(setq evil-search-module 'evil-search
      ;evil-want-C-u-scroll t
      ;evil-want-C-w-in-emacs-state t)

;(require 'evil)
;(evil-mode t)

;;;;
;;;; C-c as general purpose escape key sequence.
;(defun my-esc (prompt)
  ;"Functionality for escaping generally.  Includes exiting Evil insert state and C-g binding. "
  ;(cond
   ;((or (evil-insert-state-p)
        ;(evil-normal-state-p)
        ;(evil-replace-state-p)
        ;(evil-visual-state-p)) [escape])
   ;(t (kbd "C-g"))))

;(define-key key-translation-map (kbd "C-c") 'my-esc)
;(define-key evil-operator-state-map (kbd "<C-c>") 'keyboard-quit)
;(set-quit-char "C-c")

;;;; Some keybindings


;(load "~/.emacs.d/vim-keymaps")
;;;;; EVIL ;;;;; END
