"""""""""""""""""""" PLUGINS
set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" My Bundles here:
"
" original repos on github
Plugin 'Lokaltog/vim-easymotion'
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
Plugin 'L9'
Plugin 'FuzzyFinder'

Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/nerdcommenter'
Plugin 'tpope/vim-abolish'
Plugin 'sjl/gundo.vim'
Plugin 'auto-pairs'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-fugitive'
Plugin 'junegunn/vim-easy-align'
Plugin 'msanders/snipmate.vim'
Plugin 'vim-scripts/DfrankUtil'
Plugin 'ctrlp.vim'
Plugin 'othree/html5.vim'
Plugin 'Syntastic'
Plugin 'fatih/vim-go'
Plugin 'mxw/vim-jsx'
Plugin 'pangloss/vim-javascript'
"Plugin 'Valloric/YouCompleteMe'
"
"
"
"Themes
Plugin 'flazz/vim-colorschemes'
Plugin 'morhetz/gruvbox'
Plugin 'zeis/vim-kolor'
Plugin 'vim-scripts/guicolorscheme.vim'

call vundle#end()

" syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
      "
      "
set completeopt-=preview


"""""""""""""""""""" KEYBINDINGS
let mapleader = ','
let maplocalleader = '/'

nnoremap <F12> :tabnext <ENTER>
nnoremap <F11> :tabprevious <ENTER>
nnoremap <F10> :tabnew<SPACE>

nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>

map <C-c> <leader>c<space>
map  <F7> :setlocal spell! spelllang=
"map  <F12> :set invhls<CR>
cmap <C-g> <C-u><ESC>
command! -bang W w<bang>

" clear screen from highlights
nnoremap <silent> <C-l> :nohl<CR><C-l>

nnoremap <leader>t :ConqueTerm<SPACE>
nnoremap <leader>z :ConqueTerm zsh<CR>
nnoremap <leader>u :ConqueTermTab tmux<CR>


"""""""""""""""""""""""""""""""""""
""" INSTANTLY BETTER VIM 2013   """

"====[ Make the 81st column stand out ]====================
" just the 81st column of wide lines...
highlight ColorColumn ctermbg=magenta
call matchadd('ColorColumn', '\%81v', 100)

"====[ Make tabs, trailing whitespace, and non-breaking spaces visible ]======

exec "set listchars=tab:\uBB\uBB,trail:\uB7,nbsp:~"
set list

"====[ Swap : and ; to make colon commands easier to type ]======

nnoremap  ;  :
nnoremap  :  ;

" Another addition, ;; to <ESC> in insertmode
inoremap ;; <ESC>
vnoremap ;; <ESC>

"====[ Swap v and CTRL-V, because Block mode is more useful that Visual mode "]======

nnoremap    v   <C-V>
nnoremap <C-V>     v

vnoremap    v   <C-V>
vnoremap <C-V>     v

" dragvisual stfufs
vmap <expr> <LEFT>  DVB_Drag('left')
vmap <expr> <RIGHT> DVB_Drag('right')
vmap <expr> <DOWN>  DVB_Drag('down')
vmap <expr> <UP>    DVB_Drag('up')
vmap <expr> D DVB_Duplicate()

"""""""""""""""""""" GLOBAL
filetype plugin indent on
syntax on

set t_Co=256
colorscheme molokai
set background=dark
"set gfn=inconsolata
set go=
set ff=unix

"set nohlsearch
set encoding=utf-8
set hidden
set showcmd
set nowrap
set backspace=indent,eol,start
set autoindent
set copyindent
set number
set shiftround
set ignorecase
set smartcase
set incsearch
set history=1000
set undolevels=1000
set wildignore=*.swp,*.bak
set title
set noerrorbells
set list
set listchars=tab:>.,extends:#,nbsp:.
set ttyfast
set mouse=a
"set backup
"set backupdir=~/.vim_backup
set noswapfile
set fileformats=unix,dos,mac
set laststatus=4
set expandtab
set softtabstop=4 tabstop=4 shiftwidth=4
set ruler
set renderoptions=type:directx
set encoding=utf-8
"set guifont=MonacoForPowerline\ 11
"set guifont=FiraCode:h14
"set guifont=DroidSansMono\ 10
set guifont=DejaVuSansMono\ 12
"set guifont=Ubuntu
set et
"set tw=80
"set noexpandtab
set relativenumber
"
"
autocmd FileType go set expandtab
