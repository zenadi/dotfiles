# Fortune for you!
function fish_greeting
	fortune
end

set -gx PATH $HOME/.local/bin $PATH
set -gx EDITOR vim


############### GCLOUD
if test -e '/home/zeapo/Downloads/google-cloud-sdk/path.bash.inc'
  bass source '/home/zeapo/Downloads/google-cloud-sdk/path.bash.inc'
  bass source '/home/zeapo/Downloads/google-cloud-sdk/completion.bash.inc'
end
#########################
